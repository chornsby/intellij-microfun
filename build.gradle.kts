import org.jetbrains.grammarkit.tasks.GenerateLexer
import org.jetbrains.grammarkit.tasks.GenerateParser
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "com.chornsby"
version = "0.1.0"

buildscript {
    dependencies {
        classpath(kotlin("gradle-plugin", "1.3.31"))
    }
}

repositories {
    mavenCentral()
}

plugins {
    idea
    id("org.jetbrains.intellij") version "0.4.8"
    id("org.jetbrains.grammarkit") version "2019.1"
    kotlin("jvm") version "1.3.31"
}

sourceSets["main"].java {
    srcDirs("src/gen")
}

intellij {
    instrumentCode = true
    version = "2019.1.3"

    setPlugins("PsiViewer:191.4212")
}

val generateMicrofunLexer = task<GenerateLexer>("generateMicrofunLexer") {
    source = "src/main/grammar/microfun.flex"
    targetDir = "src/gen/com/chornsby"
    targetClass = "MicrofunLexer"
    purgeOldFiles = true
}

val generateMicrofunParser = task<GenerateParser>("generateMicrofunParser") {
    source = "src/main/grammar/microfun.bnf"
    targetRoot = "src/gen"
    pathToParser = "/com/chornsby/MicrofunParser.java"
    pathToPsiRoot = "/com/chornsby/psi"
    purgeOldFiles = true
}

tasks.withType<KotlinCompile> {
    dependsOn(generateMicrofunLexer, generateMicrofunParser)
}
