package com.chornsby

import com.chornsby.psi.MicrofunTypes
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors
import com.intellij.openapi.editor.colors.TextAttributesKey
import com.intellij.openapi.fileTypes.SingleLazyInstanceSyntaxHighlighterFactory
import com.intellij.openapi.fileTypes.SyntaxHighlighter
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase
import com.intellij.psi.tree.IElementType

class MicrofunSyntaxHighlighterFactory : SingleLazyInstanceSyntaxHighlighterFactory() {
    override fun createHighlighter(): SyntaxHighlighter = MicrofunSyntaxHighlighter()

    private class MicrofunSyntaxHighlighter : SyntaxHighlighterBase() {
        override fun getHighlightingLexer() = MicrofunLexerAdapter()

        override fun getTokenHighlights(tokenType: IElementType): Array<TextAttributesKey> {
            return MAP.getOrDefault(tokenType, EMPTY_KEYS)
        }

        companion object {
            val COMMA = TextAttributesKey.createTextAttributesKey(
                "MICROFUN_COMMA",
                DefaultLanguageHighlighterColors.COMMA
            )
            val COMMENT = TextAttributesKey.createTextAttributesKey(
                "MICROFUN_COMMENT",
                DefaultLanguageHighlighterColors.LINE_COMMENT
            )
            val DOT = TextAttributesKey.createTextAttributesKey(
                "MICROFUN_DOT",
                DefaultLanguageHighlighterColors.DOT
            )
            val IDENTIFIER = TextAttributesKey.createTextAttributesKey(
                "MICROFUN_IDENTIFIER",
                DefaultLanguageHighlighterColors.IDENTIFIER
            )
            val KEYWORD = TextAttributesKey.createTextAttributesKey(
                "MICROFUN_KEYWORD",
                DefaultLanguageHighlighterColors.KEYWORD
            )
            val NUMBER = TextAttributesKey.createTextAttributesKey(
                "MICROFUN_NUMBER",
                DefaultLanguageHighlighterColors.NUMBER
            )

            val EMPTY_KEYS = emptyArray<TextAttributesKey>()

            val MAP: Map<IElementType, Array<TextAttributesKey>> = mapOf(
                MicrofunTypes.COMMA to arrayOf(COMMA),
                MicrofunTypes.COMMENT to arrayOf(COMMENT),
                MicrofunTypes.IN to arrayOf(KEYWORD),
                MicrofunTypes.LET to arrayOf(KEYWORD),
                MicrofunTypes.NAME to arrayOf(IDENTIFIER),
                MicrofunTypes.NUMBER to arrayOf(NUMBER),
                MicrofunTypes.PERIOD to arrayOf(DOT)
            )
        }
    }
}
