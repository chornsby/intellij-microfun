package com.chornsby

import com.intellij.openapi.fileTypes.LanguageFileType

object MicrofunFileType : LanguageFileType(MicrofunLanguage) {
    override fun getName() = "Microfun file"

    override fun getDescription() = "Microfun file"

    override fun getDefaultExtension() = "mf"

    override fun getIcon() = MicrofunIcons.FILE
}
