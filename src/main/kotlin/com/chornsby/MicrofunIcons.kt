package com.chornsby

import com.intellij.openapi.util.IconLoader

object MicrofunIcons {
    val FILE = IconLoader.getIcon("fileTypes/manifest.svg")
}
