package com.chornsby.psi

import com.chornsby.MicrofunLanguage
import com.intellij.psi.tree.IElementType

class MicrofunTokenType(debugName: String) : IElementType(debugName, MicrofunLanguage)
