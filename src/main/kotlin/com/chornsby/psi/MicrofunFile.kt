package com.chornsby.psi

import com.chornsby.MicrofunFileType
import com.chornsby.MicrofunLanguage
import com.intellij.extapi.psi.PsiFileBase
import com.intellij.openapi.fileTypes.FileType
import com.intellij.psi.FileViewProvider

class MicrofunFile(viewProvider: FileViewProvider) : PsiFileBase(viewProvider, MicrofunLanguage) {
    override fun getFileType(): FileType = MicrofunFileType

    override fun toString(): String = "Microfun File"
}
