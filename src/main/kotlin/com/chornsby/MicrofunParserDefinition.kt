package com.chornsby

import com.chornsby.psi.MicrofunFile
import com.chornsby.psi.MicrofunTypes
import com.intellij.lang.ASTNode
import com.intellij.lang.ParserDefinition
import com.intellij.openapi.project.Project
import com.intellij.psi.FileViewProvider
import com.intellij.psi.tree.IFileElementType
import com.intellij.psi.tree.TokenSet

class MicrofunParserDefinition : ParserDefinition {
    override fun createLexer(project: Project?) = MicrofunLexerAdapter()

    override fun getCommentTokens() = COMMENTS

    override fun getStringLiteralElements() = TokenSet.EMPTY

    override fun createParser(project: Project?) = MicrofunParser()

    override fun getFileNodeType() = FILE

    override fun createFile(viewProvider: FileViewProvider) = MicrofunFile(viewProvider)

    override fun createElement(node: ASTNode?) = MicrofunTypes.Factory.createElement(node)

    companion object {
        val COMMENTS = TokenSet.create(MicrofunTypes.COMMENT)

        val FILE = IFileElementType(MicrofunLanguage)
    }
}
