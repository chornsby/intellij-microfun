package com.chornsby;

import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;
import com.intellij.psi.TokenType;

import static com.chornsby.psi.MicrofunTypes.*;

%%

%class MicrofunLexer
%implements FlexLexer
%unicode
%function advance
%type IElementType
%eof{  return;
%eof}

COMMENT="--" [^\r\n]*
NAME=[a-zA-Z_][a-zA-Z0-9_]*
NUMBER=[0-9]+
WHITE_SPACE=\s+

%%

"("               { return PAREN_OPEN; }
")"               { return PAREN_CLOSE; }
","               { return COMMA; }
"->"              { return ARROW; }
"."               { return PERIOD; }
"<"               { return ANGLE_OPEN; }
"="               { return ASSIGN; }
">"               { return ANGLE_CLOSE; }
"["               { return SQUARE_OPEN; }
"]"               { return SQUARE_CLOSE; }
"in"              { return IN; }
"let"             { return LET; }
"{"               { return BRACE_OPEN; }
"}"               { return BRACE_CLOSE; }

{COMMENT}         { return COMMENT; }
{NAME}            { return NAME; }
{NUMBER}          { return NUMBER; }
{WHITE_SPACE}     { return TokenType.WHITE_SPACE; }

[^] { return TokenType.BAD_CHARACTER; }
